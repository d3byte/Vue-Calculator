import Vue from 'vue';
import Vuetify from 'vuetify';

import { store } from './store/store';
import Grid from './components/Grid.vue';

Vue.use(Vuetify);

new Vue({
  el: "#app",
  store,
  render: h => h(Grid)
});
