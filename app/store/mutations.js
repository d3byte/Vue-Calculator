export default {
  newNum(state, val) {
    var operators = ['+', '-', '/', '*', '%'];
    if(!operators.includes(val)) {
      if(state.number != 'Infinity' || state.number != '-Infinity')
        state.number != 0 ? state.number += val : state.number = val;
      else
        state.number = val;
    } else
      state.number += ' ' + val + ' ';
  },
  equal(state) {
    state.number = eval(state.number);
  },
  clear(state) {
    state.number = 0;
  }
}
