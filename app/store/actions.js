export default {
  add({ commit }, val) {
    commit('add', val);
  },
  newNum({ commit }, val) {
    commit('newNum', val);
  },
  equal({ commit }) {
    commit('equal');
  },
  clear({ commit }) {
    commit('clear');
  }
}
